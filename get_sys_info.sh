#!/bin/bash
export PATH=/sbin:/usr/sbin:/bin:/usr/bin

ARCH=`/bin/uname -m`
OS=`uname`

if [[ "$ARCH" =~ ppc  ]] && [[ "$OS" =~ Linux ]]
then
        /usr/bin/yum -q -y install lshw 2>&1>/dev/null
        CMD="/usr/sbin/lshw"
fi


# check for install date
if [[ -f "/root/anaconda-ks.cfg" ]]
then
        INSTDATE=`ls --full-time /root/anaconda-ks.cfg | cut -f6,7 -d' ' | sed -e "s/\..*//"`
elif [[ -d "/root/inst-sys" ]]
then
        INSTDATE=`ls -d --full-time /root/inst-sys | cut -f6,7 -d' ' | sed -e "s/\..*//"`
else
        INSTDATE=`date +%F' '%T`
fi



declare -a array=()
if [[ -x "/usr/sbin/dmidecode" ]]
then
        while read line; do

                if [[ "$line" =~ "Manufacturer" ]]
                then
                        MANU=`echo $line | cut -f2 -d' ' | sed -e "s/\, //"`
                        array+=("$MANU")
                elif [[ "$line" =~ "Product Name" ]]
                then
                        MAKE=`echo $line | cut -f2 -d':' | sed -e "s/^ //"`
                        array+=("$MAKE")
                elif [[ "$line" =~ "Serial" ]]
                then
                        SERIAL=`echo $line | cut -f2 -d':' | sed -e "s/^ //"`
                        array+=("$SERIAL")
                fi
        done < <(dmidecode -t  system )
elif [[ -x "/usr/sbin/lshw" ]]
then
        MANU=`/usr/sbin/lshw -c system 2>/dev/null | grep serial | cut -f1 -d',' | cut -f2 -d':' | sed -e 's/^[ \t]*//'`
        array+=("$MANU")
        MODEL=`/usr/sbin/lshw -c system 2>/dev/null | grep product | cut -f2 -d','`
        array+=("$MODEL")
        SERIAL=`/usr/sbin/lshw -c system 2>/dev/null | grep serial | cut -f2 -d','`
        array+=("$SERIAL")
fi
if [[ -f "/etc/redhat-release" ]]
then
        OS=`cat /etc/redhat-release`
elif [[ -f "/etc/SuSE-release" ]]
then
        OS=`head -1 /etc/SuSE-release`
else
        OS=`uname -svr`
fi
array=( "${array[@]}" "$OS" )

INTF=`route | grep default | tr -s [:space:] | cut -f8 -d' '`
#INTF=`ifconfig -a | grep " Link" | cut -f1 -d' ' | head -1`

#ip and mac
if [[ "$OS" =~ AIX ]]
then
        INT=`netstat -r | grep default | tr -s " " | cut -f6 -d' '`
        MAC=`netstat -ia | grep $INT | head -1 | tr -s " " | cut -f4 -d' ' | tr . :`
        IP=`ifconfig en0 | grep inet | tr -s " " | tr -d "\t" | cut -f2 -d' '`
elif [ -x "/sbin/ip" ] && [[ "$OS" =~ Linux ]]
then
        IP=`/sbin/ip -f inet addr show $INTF | /bin/grep inet | cut -f6 -d' ' | head -1 | sed -e "s/\/[0-9][0-9]//"`
        array+=("$IP")
        MAC=`/sbin/ip link show $INTF | /bin/grep ether | cut -f6 -d' '`
        array+=("$MAC")

fi

echo "insert into newhosts (\"make\",\"model\",\"serial\",\"os\",\"ipaddr\",\"mac\",\"hostname\",\"created_at\",\"updated_at\") values (\"${array[0]}\",\"${array[1]}\",\"${array[2]}\",\"${array[3]}\",\"${array[4]}\",\"${array[5]}\",\"`hostname`\",\"$INSTDATE\",\"`date +%F' '%T`\");"
