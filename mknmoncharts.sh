#!/bin/bash

WWWROOT=/var/www/html/nmon
NMONFILESDIR=/root/nmonstats
CHARTEXE=/root/nmonstats/nmonchart/nmonchart

HTMLFILE=${WWWROOT}/index.html
HTMLTITLE="nmon charts"
HTMLH1="nmon reports"

echo "Writing HTML to $WWWROOT"

echo "<html>" > $HTMLFILE
echo "<title>${HTMLTITLE}</title>" >> $HTMLFILE
echo "  <h1>${HTMLH1}</h1>" >> $HTMLFILE

cd $NMONFILESDIR

for i in *.nmon
do
   BASE=`basename $i .nmon`
   IN=${NMONFILESDIR}/$i
   ##OUTDIR=${WWWROOT}/${BASE}
   ##OUT=${OUTDIR}/index.html
   OUT=${WWWROOT}/${BASE}.html
   URL=${BASE}.html

##   mkdir -p $OUTDIR
   $CHARTEXE $IN $OUT
   ##echo "<li><a href=\"${OUTDIR}\">${BASE}</a></li>" >> $HTMLFILE
   echo "  <li><a href=\"${URL}\">${BASE}</a></li>" >> $HTMLFILE

done
echo "</html>" >> $HTMLFILE

