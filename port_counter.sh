#!/bin/bash +x
function help
{
   # Display Help
   echo "Counts the number of active ports on switches in ip range."
   echo
   echo "Usage: $(basename $0) [-ich]" 2>&1
   echo "   -i     ip range xxx.xxx.xxx.xxx/xx."
   echo "   -c     SNMP Community to use."
   echo "   -h     Help."
   echo
}
if [[ ${#} -eq 0 ]]; then
   help
   exit 1
fi

while [ ! -z "$1" ]; do
  case "$1" in
     --iprange|-i)
         shift
         IP_RANGE=$1
         ;;
     --community|-c)
         shift
         COMMUNITY=$1
         ;;
     --help|-h)
        help
        exit;;
     *)
        help
        ;;
  esac
shift
done
echo "IP RANGE: $IP_RANGE"
echo "COMMUNITY: $COMMUNITY"
switches=$(nmap -sn $IP_RANGE -n | grep report |grep -o '[^ ]*$')
for switch in $switches; do
        #desc=$( $switch);
        ports_up=$(snmpwalk -c $COMMUNITY -v 2c $switch IF-MIB::ifOperStatus | grep up | wc -l)
        dns_name=$(nslookup $switch)
        echo "On switch $switch($dns_name) $ports_up are up" >> ports_up
done;

exit 0
